import numpy as np
import scipy.fftpack as fftStuff


#type 1
class FourierTransform() :
    def __init__(self, image):
        self.colorImage = image
        self.numRow = image.shape[0]
        self.numCol = image.shape[1]
        self.originalImage = image
        self.spectrum = np.zeros(image.shape)
        self.trinionSpectrum = np.zeros(image.shape)
        self.reconstructedTrinion = np.zeros(image.shape)


    def __TrinionMultiplication(self,first, second) :
        result = np.zeros(3)
        result[0] = first[0] * second[0] - first[1] * second[2] - first[2] * second[1]
        result[1] = first[0] * second[1] + first[1] * second[0] - first[2] * second[2]
        result[2] = first[0] * second[2] + first[1] * second[1] + first[2] * second[0]

        return result

    def ForwardTransform(self):        
        self.trinionSpectrum = np.zeros(self.colorImage.shape)

        self.originalImage =  (self.colorImage[:,:,0])
        tmp = fftStuff.fft2(self.originalImage)
        tmp = fftStuff.fftshift(tmp)

        firstPart = tmp.real/ np.sqrt(self.numRow * self.numCol)
        secondPart = tmp.imag/ np.sqrt(self.numRow * self.numCol)
        self.trinionSpectrum[:,:,0] += firstPart
        self.trinionSpectrum[:,:,1] -= (secondPart * np.sqrt(2)/2)
        self.trinionSpectrum[:,:,2] += (secondPart * np.sqrt(2)/2)

        self.originalImage =  (self.colorImage[:,:,1])

        tmp = fftStuff.fft2(self.originalImage)
        tmp = fftStuff.fftshift(tmp)
        firstPart = tmp.real/ np.sqrt(self.numRow * self.numCol)
        secondPart = tmp.imag/ np.sqrt(self.numRow * self.numCol)
        self.trinionSpectrum[:,:,0] -= (secondPart * np.sqrt(2)/2)
        self.trinionSpectrum[:,:,1] += firstPart
        self.trinionSpectrum[:,:,2] -= (secondPart * np.sqrt(2)/2)


        self.originalImage =  (self.colorImage[:,:,2])
        tmp = fftStuff.fft2(self.originalImage)
        tmp = fftStuff.fftshift(tmp) 
        firstPart = tmp.real/ np.sqrt(self.numRow * self.numCol)
        secondPart = tmp.imag/ np.sqrt(self.numRow * self.numCol)

        self.trinionSpectrum[:,:,0] += (secondPart * np.sqrt(2)/2)
        self.trinionSpectrum[:,:,1] -= (secondPart * np.sqrt(2)/2)
        self.trinionSpectrum[:,:,2] += firstPart

    def SetSpectrum(self, inputTrinion):
        self.trinionSpectrum = inputTrinion


    def InverseTransform(self):  
        self.reconstructedTrinion = np.zeros(self.colorImage.shape)
        
        self.spectrum =  self.trinionSpectrum[:,:,0]
        tmp = fftStuff.fftshift(self.spectrum)
        tmp = fftStuff.fft2(tmp) 
        
        firstPart = tmp.real / np.sqrt(self.numRow * self.numCol)
        secondPart = tmp.imag / np.sqrt(self.numRow * self.numCol)

        self.reconstructedTrinion[:,:,0] += firstPart - (secondPart * np.sqrt(2)/2)
        self.reconstructedTrinion[:,:,1] -= (secondPart * np.sqrt(2)/2)
        self.reconstructedTrinion[:,:,2] += (secondPart * np.sqrt(2)/2)

        self.spectrum = self.trinionSpectrum[:,:,1]
        tmp = fftStuff.fftshift(self.spectrum)
        tmp = fftStuff.fft2(tmp) 
        
        firstPart = tmp.real / np.sqrt(self.numRow * self.numCol)
        secondPart = tmp.imag / np.sqrt(self.numRow * self.numCol)


        self.reconstructedTrinion[:,:,0] -= (secondPart * np.sqrt(2)/2)
        self.reconstructedTrinion[:,:,1] += firstPart - (secondPart * np.sqrt(2)/2)
        self.reconstructedTrinion[:,:,2] -= (secondPart * np.sqrt(2)/2)

        self.spectrum = self.trinionSpectrum[:,:,2]
        tmp = fftStuff.fftshift(self.spectrum)
        tmp = fftStuff.fft2(tmp) 
         
        firstPart = tmp.real / np.sqrt(self.numRow * self.numCol)
        secondPart = tmp.imag / np.sqrt(self.numRow * self.numCol)

        self.reconstructedTrinion[:,:,0] += (secondPart * np.sqrt(2)/2)
        self.reconstructedTrinion[:,:,1] -= (secondPart * np.sqrt(2)/2)
        self.reconstructedTrinion[:,:,2] += firstPart - (secondPart * np.sqrt(2)/2)

    def GetSpectrum(self):
        return self.trinionSpectrum

    def GetImage(self):
        return self.reconstructedTrinion